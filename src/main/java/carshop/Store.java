package carshop;

import datamanagment.DataManager;
import models.Car;
import models.Client;
import models.Order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by roman on 08.02.17.
 */
public class Store {

    public HashMap<Order, Client> contractList = new HashMap<>(256);
    public HashSet<Car> cars = new HashSet<>(32);
    public HashSet<Client> clients = new HashSet<>(256);

    private static final String FILE_CONRACTS = "contracts.txt";
    private static final String FILE_CARS = "cars.txt";
    private static final String FILE_CLIENT = "clients.txt";

    public void creatCar(int price, String model, String regNum) {
        Car car = new Car(price, model, regNum);
        cars.add(car);
    }

    public void sellCar(String model, String firstname, String lastname, String phone) throws CarNotFoundException {
        Client client = new Client(firstname, lastname, phone);
        clients.add(client);
        Car _car = null;
        for (Car car:
                cars) {
            if(car.getModel().equals(model)) {
                _car = car;
                break;
            }
        }

        if(_car != null) {
            Random random = new Random();
            Order order = new Order(_car, _car.getPrice() * 2, random.nextLong(), (short)80 );
            contractList.put(order, client);
            cars.remove(_car);
        } else {
            throw new CarNotFoundException("Car not found");
        }
    }

    public ArrayList<Order> getOrders() {
        ArrayList<Order> list = new ArrayList<>();
        for (Order order:
             contractList.keySet()) {
            System.out.println(order.toString());
            list.add(order);
        }
        return list;
    }

    public ArrayList<Car> getFreeCars() {
        ArrayList<Car> list = new ArrayList<>();

        for (Car car:
                cars) {
            System.out.println(car.getModel());
            list.add(car);
        }
        return list;
    }

    public Order getFirstOrder() {
        return contractList.keySet().iterator().next();
    }

    public void save() {
        DataManager.serialize(contractList, FILE_CONRACTS);
        DataManager.serialize(cars, FILE_CARS);
        DataManager.serialize(clients, FILE_CLIENT);
    }

    public void recover (){
        ArrayList <Car> list = new ArrayList<>();
        DataManager.deserialize(FILE_CARS, list);
        for (Car car:
                list) {
            cars.add(car);
        }

        ArrayList <Client> listClient = new ArrayList<>();
        DataManager.deserialize(FILE_CLIENT, listClient);
        for (Client client:
                listClient) {
            clients.add(client);
        }

        ArrayList <Order> contractListOne = new ArrayList<>();
        ArrayList <Client> contractListTwo = new ArrayList<>();
        DataManager.deserialize(FILE_CLIENT,  contractList);
    }

    public HashMap<Order, Client> getContractList() {
        return contractList;
    }
}

