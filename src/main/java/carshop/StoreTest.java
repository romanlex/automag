package carshop;

import models.Car;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by roman on 13.02.17.
 */
class StoreTest {

    static Store store;

    @BeforeAll
    @Deprecated
    public static void initStore() {
        store = new Store();
        assertNotNull(store);
    }

    @Test
    void creatCar() {
        store = new Store();
        assertNotNull(store);

        Car car = new Car(100, "Lada", "ABC");
        store.creatCar(100, "Lada", "ABC");

        assertNotNull(store.getFreeCars());

        assertTrue(store.getFreeCars().size() > 0);

        store.getFreeCars().stream().forEach((car1) -> {
            assertEquals(100, car1.getPrice());
            assertEquals("Lada", car1.getModel());
            assertEquals("ABC", car1.getRegNum());
        });

    }

    @Test
    void sellCar() throws CarNotFoundException {
        Store store = new Store();
        Assertions.assertThrows(CarNotFoundException.class, () -> {
            store.sellCar("GAZ", "talian", "jon", "911");
        });

        store.creatCar(100, "Lada", "ABC");

        store.sellCar("Lada", "talian", "jon", "911");

        assertTrue(store.getFreeCars().size() == 0);

        assertTrue(store.getOrders().stream().filter((order) -> order.getCar().getModel().equals("Lada")
            && order.getCar().getPrice() == 100
            && order.getCar().getRegNum().equals("ABC"))
        .count() > 0);

        assertTrue(store.getContractList().size() == 1);

        assertTrue(store.getContractList().values().stream().filter(
                (client) -> client.getFirstname().equals("talian")
                    && client.getLastname().equals("jon")
                    && client.getPhone().equals("911")
            ).count() == 1);
    }

    @Test
    void getContractList() {
        Store store = new Store();
        assertNotNull(store.getContractList());
        assertTrue(store.getContractList().size() == 0);
    }

    @Test
    void getOrders() {

    }

    @Test
    void getFreeCars() {

    }

    @Test
    void getFirstOrder() {
        Store store = new Store();
        assertNotNull(store);

//        store.getOrders().stream().forEach((order) -> {
//                assertEquals(100, car1.getPrice());
//                assertEquals("Lada", car1.getModel());
//                assertEquals("ABC", car1.getRegNum());
//        });
    }

    @Test
    void save() {

    }

    @Test
    void recover() {

    }

}