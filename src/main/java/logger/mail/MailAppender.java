package logger.mail;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * Created by roman on 14.02.17.
 */
public class MailAppender extends AppenderSkeleton {

    //private final long MAX_LOGFILE_SIZE = 1048576;
    public static final long MAX_LOGFILE_SIZE = 124;
    private static final String LOG_FILE_NAME = "logfile.rar";

    private String username;
    private String password;
    private String to;
    private String cc;
    private boolean smtpAuth;
    private boolean smtpTls;
    private String smtpServer;
    private String smtpPort;

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSmtpAuth() {
        return smtpAuth;
    }

    public void setSmtpAuth(boolean smtpAuth) {
        this.smtpAuth = smtpAuth;
    }

    public boolean isSmtpTls() {
        return smtpTls;
    }

    public void setSmtpTls(boolean smtpTls) {
        this.smtpTls = smtpTls;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(String smtpPort) {
        this.smtpPort = smtpPort;
    }

    @Override
    protected void append(LoggingEvent loggingEvent) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", smtpAuth);
        props.put("mail.smtp.starttls.enable", smtpTls);
        props.put("mail.smtp.host", smtpServer);
        props.put("mail.smtp.port", smtpPort);

        Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            if (cc.length() > 0) {
                message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc, false));
            }

            Date date = new Date();
            message.setSubject("[Olin Roman] Log File from " + date.toString());

            String messageText = layout.format(loggingEvent);
            String messageHtml = layout.format(loggingEvent);
            List<Path> attachments = new ArrayList<Path>();
            File logfile = new File("applog.txt");
            long size = logfile.length();
            if(size > MAX_LOGFILE_SIZE)
            {
                String output = executeCommand("rar a " + LOG_FILE_NAME + " " + logfile.getCanonicalPath() + " -ep1");
                System.out.println(output);
                if(output.contains("Done")) {
                    //logfile.delete();
                    //executeCommand("touch applog.txt");
                }
            }

            Path archivePath = Paths.get(new File("logfile.rar").getCanonicalPath());
            if(Files.exists(archivePath)) {
                attachments.add(archivePath);
            }

            // create mixed mail body with TXT and HTML
            MailBuilder mailBuilder = new MailBuilder();
            Multipart mpMixed = mailBuilder.build(messageText, messageHtml, attachments);
            message.setContent(mpMixed);
            message.setSentDate(date);

            System.out.println("Sending");
            Transport.send(message);
            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                File archive = new File(LOG_FILE_NAME);
                if(Files.exists(Paths.get(archive.getCanonicalPath()))) {
                    archive.delete();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String executeCommand(String command) {
        StringBuilder output = new StringBuilder();
        BufferedReader reader = null;
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = reader.readLine())!= null) {
                output.append(line + "\n");
            }
            p.waitFor();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }

    @Override
    public void close() {

    }

    @Override
    public boolean requiresLayout() {
        return false;
    }
}
