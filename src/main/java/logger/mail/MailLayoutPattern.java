package logger.mail;

import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Created by roman on 14.02.17.
 */
public class MailLayoutPattern extends PatternLayout {

    public String format(LoggingEvent event) {
        Object obj = (Object)event.getMessage();

        StringBuffer sb = new StringBuffer();

        System.out.println(obj.toString());

        sb.append("Sender: Olin Roman\n" +
                "Group: SCT_04\n" +
                "Theme: Send log in archive to recipient\n" +
                "Bitbucket repo: https://romanlex@bitbucket.org/romanlex/automag.git\n" +
                "Max file size to pack in RAR and send setted to: " + humanReadableByteCount(MailAppender.MAX_LOGFILE_SIZE, true) + "\n" +
                "In attachment you can find archive with last log file.\nLog was deleted from hard disk. This copy is last of log.\n");

//        sb.append("<name>").append(name).append("</name>");
//        sb.append("<age>").append(age).append("</age>");
//        sb.append("<country>").append(country).append("</country>");

        return sb.toString();
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
}
