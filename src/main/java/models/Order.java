package models;

import java.io.Serializable;

/**
 * Created by roman on 08.02.17.
 */
public class Order implements Serializable {
    private Car car;
    private int cost;
    private long orderNumber;
    private short orgNumber;

    public Order(Car car, int cost, long orderNumber, short orgNumber) {
        this.car = car;
        this.cost = cost;
        this.orderNumber = orderNumber;
        this.orgNumber = orgNumber;
    }

    public short getOrgNumber() {
        return orgNumber;
    }

    public void setOrgNumber(short orgNumber) {
        this.orgNumber = orgNumber;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(long orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Override
    public int hashCode() {
        return (int) (orderNumber*21 + orgNumber);
    }


    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return false;
        }

        if(o == this) {
            return  true;
        }

        if(!(o instanceof Order)) {
            return false;
        }

        Order order = (Order)o;
        if(this.orderNumber == order.getOrderNumber()
                && this.orgNumber == order.getOrderNumber()) {
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return car.getModel() + " RegNum:"
                + car.getRegNum()
                + " Cost:" + cost
                + " orderNum:" + orderNumber
                + " orgNumber:" + orgNumber;
    }
}
