package models;

import java.io.Serializable;

/**
 * Created by roman on 08.02.17.
 */
public class Car implements Serializable {
    private int price;
    private String model;
    private String regNum;

    public Car(int price, String model, String regNum) {
        this.price = price;
        this.model = model;
        this.regNum = regNum;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }

    //Auto generate
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (price != car.price) return false;
        if (model != null ? !model.equals(car.model) : car.model != null) return false;
        return regNum != null ? regNum.equals(car.regNum) : car.regNum == null;
    }

    @Override
    public int hashCode() {
        int result = price;
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (regNum != null ? regNum.hashCode() : 0);
        return result;
    }

//    @Override
//    public String toString() {
//        return car.getModel() + " RegNum:"
//                + car.getRegNum()
//                + " Cost:" + cost
//                + " orderNum:" + orderNumber
//                + " orgNumber:" + orgNumber;
//    }
}
