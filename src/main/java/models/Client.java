package models;

import java.io.Serializable;

/**
 * Created by roman on 08.02.17.
 */
public class Client implements Serializable {
    private String firstname;
    private String lastname;
    private String phone;

    public Client(String firstname, String lastname, String phone) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int hashCode() {
        return firstname.hashCode()*21
                + lastname.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;

        if(!(o instanceof Client))
            return false;

        Client client = (Client)o;

        if(this.getFirstname().equals(client.firstname)
                && this.getLastname().equals((client.lastname)))
            return true;

        return false;
    }
}
